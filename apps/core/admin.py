# Django
from django.contrib import admin

# Models
from .models import Service, Price


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    """Service admin."""

    list_display = ('title', 'text', 'icon')
    search_fields = ('title',)
    list_filter = ('created', 'modified')

@admin.register(Price)
class PriceAdmin(admin.ModelAdmin):
    """Price admin."""

    list_display = ('title', 'price',)
    search_fields = ('title',)
    list_filter = ('created', 'modified')