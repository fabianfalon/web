# Create your models here.
from django.db import models


class Service(models.Model):
    """Service model."""

    title = models.CharField(max_length=255)
    text = models.TextField()
    icon = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return title."""
        return '{}'.format(self.title)


class Price(models.Model):
    """Price model."""

    title = models.CharField(max_length=255)
    price = models.CharField(max_length=255)
    description1 = models.CharField(max_length=255)
    description2 = models.CharField(max_length=255)
    description3 = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return title."""
        return '{}'.format(self.title)
        