from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Service, Price
# Create your views here.

class HomeView(TemplateView):
	template_name = 'index.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		context['services'] = Service.objects.all()
		context['prices'] = Price.objects.all()
		return context